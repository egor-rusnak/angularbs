﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TasksBs.BLL.Interfaces;
using TasksBs.Common.DTOs;
using TasksBs.Common.DTOs.Project;
using TasksBs.Common.DTOs.User;

namespace TasksBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QueriesController : ControllerBase
    {
        private readonly IQueryService _queryService;

        public QueriesController(IQueryService queryService)
        {
            _queryService = queryService;
        }

        [HttpGet("First/{id}")]
        public async Task<ActionResult<Dictionary<ProjectDto, int>>> FirstQuery(int id)
        {
            var result = await _queryService.ExecuteQueryOne(id);

            return Ok(result.Select(e => e));
        }

        [HttpGet("Second/{id}")]
        public async Task<ActionResult<IEnumerable<TaskDto>>> SecondQuery(int id)
        {
            var result = await _queryService.ExecuteQueryTwo(id);

            return Ok(result);
        }

        [HttpGet("Third/{id}")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<int, string>>>> ThirdQuery(int id)
        {
            var result = await _queryService.ExecuteQueryThree(id);

            return Ok(result);
        }

        [HttpGet("Fourth")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>>>> FourthQuery()
        {
            var result = await _queryService.ExecuteQueryFour();

            return Ok(result);
        }

        [HttpGet("Fifth")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<UserDto, IEnumerable<TaskDto>>>>> FifthQuery()
        {
            var result = await _queryService.ExecuteQueryFive();

            return Ok(result);
        }

        [HttpGet("Sixth/{id}")]
        public async Task<ActionResult<UserInfoDto>> SixthQuery(int id)
        {
            try
            {
                var result = await _queryService.ExecuteQuerySix(id);
                return Ok(result);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("Seventh")]
        public async Task<ActionResult<IEnumerable<ProjectInfoDto>>> SeventhQuery()
        {
            var result = await _queryService.ExecuteQuerySeven();

            return Ok(result);
        }
    }
}
