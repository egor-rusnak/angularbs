﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TasksBs.BLL.Interfaces;
using TasksBs.Common.DTOs;

namespace TasksBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDto>>> GetAll()
        {
            return Ok(await _taskService.GetAllTasks());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDto>> Get(int id)
        {
            try
            {
                var task = await _taskService.GetById(id);
                return Ok(task);
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPost]
        public async Task<ActionResult<TaskDto>> CreateTask([FromBody] TaskDto task)
        {
            if (!ModelState.IsValid)
                return BadRequest("The data is not valid!");

            try
            {
                var result = await _taskService.AddTask(task);
                return CreatedAtAction(nameof(Get), new { id = result.Id }, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPut]
        public async Task<ActionResult<TaskDto>> UpdateTask([FromBody] TaskDto task)
        {
            if (!ModelState.IsValid)
                return BadRequest("The data is not valid!");

            try
            {
                var result = await _taskService.UpdateTask(task);
                return Ok(result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTask(int id)
        {
            try
            {
                await _taskService.RemoveTask(id);
                return NoContent();
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPatch("finished/{id}")]
        public async Task<IActionResult> SetTaskAsFinished(int id)
        {
            try
            {
                await _taskService.SetTaskAsFinished(id);
                return NoContent();
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpGet("unfinishedByUser/{userId}")]
        public async Task<ActionResult<IEnumerable<TaskDto>>> GetUnfinishedTasksByUserId(int userId)
        {
            try
            {
                var result = await _taskService.GetUnfinishedTasksByUser(userId);
                return Ok(result);
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }
    }
}
