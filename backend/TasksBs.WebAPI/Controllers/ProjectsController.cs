﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TasksBs.BLL.Interfaces;
using TasksBs.Common.DTOs.Project;

namespace TasksBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDto>>> GetAll()
        {
            return Ok(await _projectService.GetAllProjects());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDto>> Get(int id)
        {
            try
            {
                var project = await _projectService.GetById(id);
                return Ok(project);
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDto>> CreateProject([FromBody] ProjectDto project)
        {
            if (!ModelState.IsValid)
                return BadRequest("The data is not valid!");

            try
            {
                var result = await _projectService.AddProject(project);
                return CreatedAtAction(nameof(Get), new { id = result.Id }, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPut]
        public async Task<ActionResult<ProjectDto>> UpdateProject([FromBody] ProjectDto project)
        {
            if (!ModelState.IsValid)
                return BadRequest("The data is not valid!");
            try
            {
                var result = await _projectService.UpdateProject(project);
                return Ok(result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProject(int id)
        {
            try
            {
                await _projectService.RemoveProject(id);
                return NoContent();
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

    }
}
