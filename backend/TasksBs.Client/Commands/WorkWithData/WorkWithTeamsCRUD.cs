﻿using System;
using System.Threading.Tasks;
using TasksBs.Common.DTOs;
using TasksBs.UI.Model.Interfaces;
using static TasksBs.UI.Tools.InputWorker;

namespace TasksBs.UI.Commands.WorkWithData
{
    public class WorkWithTeamsCRUD : WorkWithDataCRUD<TeamDto>
    {
        public WorkWithTeamsCRUD(ICRUDService<TeamDto> service) : base(service)
        {
            Description = "Teams";
        }

        public override async Task<TeamDto> CreateEntity()
        {
            var team = new TeamDto()
            {
                CreatedAt = DateTime.Now,
                Name = GetString("Enter team name: ")
            };
            return await Task.FromResult(team);
        }

        public override async Task<TeamDto> UpdateEntity(TeamDto entity)
        {
            entity.Name = GetString($"Enter new team name (old: {entity.Name}): ");
            return await Task.FromResult(entity);
        }
    }
}
