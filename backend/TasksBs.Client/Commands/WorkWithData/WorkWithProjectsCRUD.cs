﻿using System;
using System.Threading.Tasks;
using TasksBs.Common.DTOs;
using TasksBs.Common.DTOs.Project;
using TasksBs.Common.DTOs.User;
using TasksBs.UI.Model.Interfaces;
using static TasksBs.UI.Tools.InputWorker;

namespace TasksBs.UI.Commands.WorkWithData
{
    public class WorkWithProjectsCRUD : WorkWithDataCRUD<ProjectDto>
    {
        private readonly ICRUDService<UserDto> _userService;
        private readonly ICRUDService<TeamDto> _teamService;

        public WorkWithProjectsCRUD(ICRUDService<ProjectDto> service, ICRUDService<UserDto> userService, ICRUDService<TeamDto> teamService) : base(service)
        {
            Description = "Projects";
            _userService = userService;
            _teamService = teamService;
        }

        public override async Task<ProjectDto> CreateEntity()
        {
            var project = new ProjectDto()
            {
                CreatedAt = DateTime.Now,
                Name = GetString("Enter project name: "),
                Description = GetString("Enter project Description: "),
                Deadline = GetDate("Enter Deadline date: ").Value,
                Author = await Input(_userService, "Choose author id:"),
                Team = await Input(_teamService, "Choose team id:")
            };
            return project;
        }

        public override async Task<ProjectDto> UpdateEntity(ProjectDto entity)
        {
            entity.Name = GetString($"Enter new project name (old: {entity.Name}): ");
            entity.Description = GetString($"Enter new project Description (old: {entity.Description}): ");
            entity.Deadline = GetDate($"Enter new Deadline date (old: {entity.Deadline}): ").Value;
            entity.Author = await Input(_userService, $"Input a new author id (old: {entity.Author}): ");
            entity.Team = await Input(_teamService, $"Input new team id (old: {entity.Team}): ");

            return entity;
        }
    }
}
