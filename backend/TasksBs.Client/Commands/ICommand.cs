﻿using System.Threading.Tasks;

namespace TasksBs.UI.Commands
{
    public interface ICommand
    {
        string Description { get; set; }
        Task Execute();
    }
}
