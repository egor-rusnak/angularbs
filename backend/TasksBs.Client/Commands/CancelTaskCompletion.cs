﻿using System;
using System.Threading.Tasks;

namespace TasksBs.UI.Commands
{
    public class CancelTaskCompletion : ICommand
    {
        private EventHandler _cancelerEvent;
        public CancelTaskCompletion(EventHandler cancelerEvent)
        {
            _cancelerEvent = cancelerEvent;
        }

        public string Description { get; set; } = "Cancel task completion";

        public async Task Execute()
        {
            _cancelerEvent?.Invoke(this, null);
            await Task.CompletedTask;
        }
    }
}
