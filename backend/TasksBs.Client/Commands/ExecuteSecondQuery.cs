﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TasksBs.UI.Model.Interfaces;

namespace TasksBs.UI.Commands
{
    public class ExecuteSecondQuery : ICommand
    {
        private readonly IQueryService _projectService;
        public ExecuteSecondQuery(IQueryService service)
        {
            _projectService = service;
        }
        public string Description { get; set; } = "Execute query number 2";
        public async Task Execute()
        {
            Console.Write("Input a user Id: ");
            if (int.TryParse(Console.ReadLine(), out int input))
            {
                Console.WriteLine($"Tasks for user [{input}] where task name length < 45: ");
                var result = await _projectService.ExecuteQueryTwo(input);
                foreach (var elem in result)
                {
                    Console.WriteLine($"{elem}");
                }
                if (result.Count() == 0) Console.WriteLine("BLANK");
            }

        }
    }
}
