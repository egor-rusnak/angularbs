﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TasksBs.UI.Model.Interfaces;

namespace TasksBs.UI.Commands
{
    public class StartTaskCompletion : ICommand
    {
        private CancellationTokenSource source;
        private readonly IQueryService _service;
        public StartTaskCompletion(IQueryService service, ref EventHandler cancelEvent)
        {
            _service = service;
            cancelEvent += OnCancel;
        }
        public string Description { get; set; } = "Start task completion (1 sec delay)";

        public async Task Execute()
        {
            source = new CancellationTokenSource();
            var token = source.Token;

            _ = Task.Run(async () =>
            {
                Console.WriteLine("Started finishing tasks periodicaly...");
                while (true)
                {
                    int markedTaskId = await _service.MarkRandomTaskWithDelay(1000);
                    if (markedTaskId == 0)
                        Console.WriteLine("No unfinished tasks!");
                    else
                        Console.WriteLine("\n====Finished task id:  " + markedTaskId + "=====");

                    if (token.IsCancellationRequested)
                    {
                        Console.WriteLine("Stopping background task..");
                        break;
                    }
                }
            }, token);

            await Task.CompletedTask;
        }

        private void OnCancel(object sender, EventArgs args)
        {
            source?.Cancel();
        }
    }
}
