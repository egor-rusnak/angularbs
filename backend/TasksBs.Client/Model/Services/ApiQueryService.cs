﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;
using TasksBs.Common.DTOs;
using TasksBs.Common.DTOs.Project;
using TasksBs.Common.DTOs.User;
using TasksBs.UI.Extensions;
using TasksBs.UI.Model.Interfaces;

namespace TasksBs.UI.Model.Services
{
    public class ApiQueryService : IQueryService
    {
        private readonly HttpClient _client;
        private readonly Timer _timer;
        private TaskCompletionSource<int> completionSource;

        public ApiQueryService(Uri apiAdress)
        {
            _client = new HttpClient()
            {
                BaseAddress = apiAdress
            };
            _timer = new Timer();
            _timer.Elapsed += WhenElapsed;
            _timer.AutoReset = false;
        }


        public void Dispose()
        {
            _client.Dispose();
            _timer.Dispose();
        }

        public async Task<IEnumerable<KeyValuePair<UserDto, List<TaskDto>>>> ExecuteQueryFive()
        {
            return await _client.HandleGetRequest<List<KeyValuePair<UserDto, List<TaskDto>>>>("api/Queries/Fifth");
        }

        public async Task<IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>>> ExecuteQueryFour()
        {
            return await _client.HandleGetRequest<List<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>>>("api/Queries/Fourth");
        }

        public async Task<Dictionary<ProjectDto, int>> ExecuteQueryOne(int userId)
        {
            return new Dictionary<ProjectDto, int>(await _client.HandleGetRequest<List<KeyValuePair<ProjectDto, int>>>($"api/Queries/First/{userId}"));
        }

        public async Task<IEnumerable<ProjectInfoDto>> ExecuteQuerySeven()
        {
            return await _client.HandleGetRequest<List<ProjectInfoDto>>("api/Queries/Seventh");
        }

        public async Task<UserInfoDto> ExecuteQuerySix(int userId)
        {
            return await _client.HandleGetRequest<UserInfoDto>($"api/Queries/Sixth/{userId}");
        }

        public async Task<IEnumerable<KeyValuePair<int, string>>> ExecuteQueryThree(int userId)
        {
            return await _client.HandleGetRequest<List<KeyValuePair<int, string>>>($"api/Queries/Third/{userId}");
        }

        public async Task<IEnumerable<TaskDto>> ExecuteQueryTwo(int userId)
        {
            return await _client.HandleGetRequest<List<TaskDto>>($"api/Queries/Second/{userId}");
        }

        public async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            _timer.Interval = delay;
            _timer.Start();

            completionSource = new TaskCompletionSource<int>();

            return await completionSource.Task;
        }

        private async void WhenElapsed(object sender, ElapsedEventArgs e)
        {
            var tasks = (await _client.HandleGetRequest<List<TaskDto>>("api/tasks")).Where(t => t.FinishedAt == null).ToList();
            if (tasks.Count == 0)
            {
                completionSource.SetResult(0);
                return;
            }

            var index = new Random().Next() % tasks.Count;
            var taskId = tasks[index].Id;

            await _client.HandlePatchRequest($"api/tasks/finished/{taskId}");
            completionSource.SetResult(taskId);
        }
    }
}
