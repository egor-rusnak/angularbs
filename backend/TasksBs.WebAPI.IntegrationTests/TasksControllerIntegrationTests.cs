﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using TasksBs.Common.DTOs;
using TasksBs.DAL.Context;
using TasksBs.TestsCommon;
using Xunit;

namespace TasksBs.WebAPI.IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private readonly CustomWebApplicationFactory<Startup> _factory;
        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = _factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async System.Threading.Tasks.Task SetTaskAsFinished_TaskSettedAsFinishedWithCodeNoContent()
        {
            int taskId = 0;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ProjectDbContext>();
                var team = await context.AddTeam();
                var user = await context.AddUser(teamId: team.Id);
                var project = await context.AddProject(user.Id, team.Id);
                var task = await context.AddTask(user.Id, project.Id);
                await context.SaveChangesAsync();
                taskId = task.Id;
            }

            var response = await _client.PatchAsync($"api/tasks/finished/{taskId}", null);

            response.EnsureSuccessStatusCode();
            Assert.Equal(System.Net.HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task SetTaskAsFinished_WrongTaskId_ReturnsNotFoundCode()
        {
            int taskId = 0;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ProjectDbContext>();
                var team = await context.AddTeam();
                var user = await context.AddUser(teamId: team.Id);
                var project = await context.AddProject(user.Id, team.Id);
                var task = await context.AddTask(user.Id, project.Id);
                await context.SaveChangesAsync();
                taskId = task.Id;
            }

            taskId++;
            var response = await _client.PatchAsync($"api/tasks/finished/{taskId}", null);

            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task RemoveTask_TaskRemovedWithNoContentCode()
        {
            int idToRemove = 0;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ProjectDbContext>();
                var team = await context.AddTeam();
                var user = await context.AddUser(teamId: team.Id);
                var project = await context.AddProject(user.Id, team.Id);
                var task = await context.AddTask(user.Id, project.Id);
                await context.SaveChangesAsync();
                idToRemove = task.Id;
            }

            var response = await _client.DeleteAsync($"api/tasks/{idToRemove}");

            response.EnsureSuccessStatusCode();
            Assert.Equal(System.Net.HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task RemoveTask_WhenNotExist_NotFoundCodeReturned()
        {
            int idToRemove = 0;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ProjectDbContext>();
                var team = await context.AddTeam();
                var user = await context.AddUser(teamId: team.Id);
                var project = await context.AddProject(user.Id, team.Id);
                var task = await context.AddTask(user.Id, project.Id);
                await context.SaveChangesAsync();
                idToRemove = task.Id;
            }

            idToRemove++;
            var response = await _client.DeleteAsync($"api/tasks/{idToRemove}");

            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUnfinishedTasksByUser_UserWithUnfinishedTasks_ReturnsCollectionOfTasksWithOkCode()
        {
            int userId = 0;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ProjectDbContext>();
                var team = await context.AddTeam();
                var user = await context.AddUser(teamId: team.Id);
                var project = await context.AddProject(user.Id, team.Id);
                await context.AddTask(user.Id, project.Id, finishedAt: DateTime.Now);
                await context.AddTask(user.Id, project.Id, finishedAt: DateTime.Now);
                await context.AddTask(user.Id, project.Id);
                await context.AddTask(user.Id, project.Id);
                await context.AddTask(user.Id, project.Id, finishedAt: DateTime.Now);
                await context.AddTask(user.Id, project.Id, finishedAt: DateTime.Now);
                await context.SaveChangesAsync();
                userId = user.Id;
            }

            var response = await _client.GetAsync($"api/tasks/unfinishedByUser/{userId}");

            response.EnsureSuccessStatusCode();
            var tasksResult = await response.Content.ReadFromJsonAsync<List<TaskDto>>();
            Assert.True(tasksResult.Count() == 2);
            Assert.All(tasksResult, t => Assert.Equal(userId, t.Performer.Id));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUnfinishedTasksByUser_NotExistingUser_ReturnsNotFoundCode()
        {
            var response = await _client.GetAsync($"api/tasks/unfinishedByUser/{99832}");

            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
