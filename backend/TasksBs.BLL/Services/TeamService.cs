﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TasksBs.BLL.Interfaces;
using TasksBs.BLL.Services.Abstraction;
using TasksBs.Common.DTOs;
using TasksBs.DAL.Context;
using TasksBs.DAL.Entities;

namespace TasksBs.BLL.Services
{
    public class TeamService : BaseSerivce, ITeamService
    {
        public TeamService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }

        public async Task<TeamDto> AddTeam(TeamDto team)
        {
            var teamEntity = _mapper.Map<Team>(team);

            await _context.Teams.AddAsync(teamEntity);

            await _context.SaveChangesAsync();
            return _mapper.Map<TeamDto>(await GetTeam(teamEntity.Id));
        }

        public async Task<TeamDto> UpdateTeam(TeamDto team)
        {
            var teamEntity = _mapper.Map<Team>(team);

            _context.Teams.Update(teamEntity);

            await _context.SaveChangesAsync();
            return _mapper.Map<TeamDto>(await GetTeam(teamEntity.Id));
        }

        public async System.Threading.Tasks.Task RemoveTeam(int id)
        {
            var teamEntity = await GetTeam(id);

            if (teamEntity == null)
                throw new ArgumentException("No entity with this id!");

            _context.Teams.Remove(teamEntity);

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<TeamDto>> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamDto>>(await GetTeams().ToListAsync());
        }

        public async Task<TeamDto> GetById(int id)
        {
            var teamEntity = await GetTeam(id);

            if (teamEntity == null)
                throw new ArgumentException("No entity with this id");

            return _mapper.Map<TeamDto>(teamEntity);
        }

        private IQueryable<Team> GetTeams()
        {
            return _context.Teams.Include(t => t.Members);
        }

        private async Task<Team> GetTeam(int id)
        {
            return await GetTeams().FirstOrDefaultAsync(t => t.Id == id);
        }

        public async System.Threading.Tasks.Task AddUserToTeam(int teamId, int userId)
        {
            var team = await _context.Teams.FirstOrDefaultAsync(t => t.Id == teamId);
            var user = await _context.Users.Include(u => u.Team).FirstOrDefaultAsync(u => u.Id == userId);

            if (team == null)
                throw new ArgumentException("No team with this id");
            if (user == null)
                throw new ArgumentException("No user with this id");
            if (user.Team != null)
                throw new ArgumentException("User already has a team!");

            user.Team = team;
            _context.Update(user);
            await _context.SaveChangesAsync();
        }
    }
}
