﻿using AutoMapper;
using TasksBs.Common.DTOs.Project;
using TasksBs.DAL.Entities;

namespace TasksBs.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDto>();
            CreateMap<ProjectDto, Project>()
                .ForMember(m => m.Author, opt => opt.Ignore())
                .ForMember(m => m.Team, opt => opt.Ignore());

        }
    }
}
