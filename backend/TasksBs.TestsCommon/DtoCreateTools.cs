﻿using System;
using TasksBs.Common.DTOs;
using TasksBs.Common.DTOs.Project;
using TasksBs.Common.DTOs.User;

namespace TasksBs.TestsCommon
{
    public static class DtoCreateTools
    {
        public static TaskDto CreateTask(
            UserDto performer,
            ProjectDto project,
            int id = 0,
            string name = "name",
            string description = "desc",
            DateTime? createdAt = null,
            DateTime? finishedAt = null)
        {
            return new TaskDto
            {
                Id = id,
                Name = name,
                Performer = performer,
                Project = project,
                Description = description,
                CreatedAt = createdAt ?? DateTime.Now,
                FinishedAt = finishedAt
            };
        }
        public static UserDto CreateUser(
            TeamDto team = null,
            int id = 0,
            string firstName = "fname",
            string lastName = "lname",
            string email = "email",
            DateTime? birthDate = null,
            DateTime? registeredAt = null
            )
        {
            return new UserDto
            {
                Id = id,
                FirstName = firstName,
                LastName = lastName,
                Team = team,
                Email = email,
                BirthDay = birthDate ?? DateTime.Now.AddYears(-10),
                RegisteredAt = registeredAt ?? DateTime.Now.AddYears(-30)
            };
        }
        public static TeamDto CreateTeam(
           int id = 0,
           string name = "teamName",
           DateTime? createdAt = null
           )
        {
            return new TeamDto
            {
                Id = id,
                Name = name,
                CreatedAt = createdAt ?? DateTime.Now
            };
        }
        public static ProjectDto CreateProject(
            UserDto author,
            TeamDto team,
            int id = 0,
            string name = "projectName",
            string description = "Description",
            DateTime? deadline = null,
            DateTime? createdAt = null
            )
        {
            return new ProjectDto
            {
                Id = id,
                Name = name,
                Description = description,
                Team = team,
                Author = author,
                Deadline = deadline ?? DateTime.Now.AddMonths(3),
                CreatedAt = createdAt ?? DateTime.Now,
            };
        }
    }
}
