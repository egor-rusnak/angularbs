﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace TasksBs.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    RegisteredAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: false),
                    Deadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Projects_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    PerformerId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_Users_PerformerId",
                        column: x => x.PerformerId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2026, 2, 28, 17, 4, 43, 302, DateTimeKind.Local).AddTicks(2100), "Bananas" },
                    { 2, new DateTime(2024, 3, 7, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(3837), "Changes" },
                    { 3, new DateTime(2017, 1, 16, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(3931), "Packages" },
                    { 4, new DateTime(2023, 2, 13, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(4078), "asdas" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 6, new DateTime(2012, 6, 2, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9450), "", "George", "Jakson", new DateTime(2014, 5, 13, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9476), null },
                    { 7, new DateTime(2017, 10, 8, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9498), "", "Annet", "Wonderson", new DateTime(2013, 10, 14, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9523), null }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, 7, new DateTime(2024, 5, 13, 17, 4, 43, 306, DateTimeKind.Local).AddTicks(4211), new DateTime(2015, 1, 14, 17, 4, 43, 306, DateTimeKind.Local).AddTicks(4148), "Des4", "ProjName4", 3 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(2017, 12, 18, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(5284), "", "John", "Jonson", new DateTime(2012, 3, 11, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(8780), 1 },
                    { 8, new DateTime(2008, 12, 27, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9546), "", "Marcus", "Jonson", new DateTime(2017, 4, 12, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9571), 1 },
                    { 4, new DateTime(2006, 1, 24, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9347), "", "Boris", "Apostolov", new DateTime(2020, 1, 27, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9374), 2 },
                    { 5, new DateTime(2016, 11, 15, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9397), "", "Grace", "Donalds", new DateTime(2017, 6, 9, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9422), 2 },
                    { 2, new DateTime(2012, 3, 21, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9233), "", "Marchel", "Davidson", new DateTime(2017, 11, 6, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9275), 3 },
                    { 3, new DateTime(2012, 2, 25, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9298), "", "Anchey", "Brownly", new DateTime(2017, 1, 25, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9325), 3 },
                    { 9, new DateTime(2009, 9, 7, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9593), "", "Marcus", "Jonson", new DateTime(2015, 2, 20, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9618), 4 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTime(2020, 5, 20, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9911), new DateTime(2022, 3, 21, 17, 4, 43, 305, DateTimeKind.Local).AddTicks(9772), "Des1", "ProjName1", 1 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, 8, new DateTime(2023, 12, 5, 17, 4, 43, 306, DateTimeKind.Local).AddTicks(4082), new DateTime(2021, 7, 6, 17, 4, 43, 306, DateTimeKind.Local).AddTicks(4016), "Des3", "ProjName3", 2 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, 5, new DateTime(2024, 11, 8, 17, 4, 43, 306, DateTimeKind.Local).AddTicks(3946), new DateTime(2017, 7, 4, 17, 4, 43, 306, DateTimeKind.Local).AddTicks(3863), "Des2", "ProjName2", 1 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { 1, new DateTime(2026, 1, 23, 17, 4, 43, 306, DateTimeKind.Local).AddTicks(4415), "no", null, "task1", 1, 1 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { 2, new DateTime(2026, 3, 27, 17, 4, 43, 306, DateTimeKind.Local).AddTicks(7637), "no", new DateTime(2015, 11, 9, 17, 4, 43, 306, DateTimeKind.Local).AddTicks(7785), "task2", 1, 1 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { 3, new DateTime(2017, 6, 27, 17, 4, 43, 306, DateTimeKind.Local).AddTicks(8281), "no", null, "task3", 1, 1 });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerformerId",
                table: "Tasks",
                column: "PerformerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
