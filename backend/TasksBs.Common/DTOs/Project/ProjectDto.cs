﻿using System;
using System.ComponentModel.DataAnnotations;
using TasksBs.Common.DTOs.Abstraction;
using TasksBs.Common.DTOs.User;

namespace TasksBs.Common.DTOs.Project
{
    public class ProjectDto : BaseEntityDto
    {
        
        public string Name { get; set; }
        public UserDto Author { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public TeamDto Team { get; set; }

        public override string ToString()
        {
            return Id + "-" + Name;
        }
    }
}
