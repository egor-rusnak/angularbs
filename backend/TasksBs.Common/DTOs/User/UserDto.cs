﻿using System;
using TasksBs.Common.DTOs.Abstraction;

namespace TasksBs.Common.DTOs.User
{
    public class UserDto : BaseEntityDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime RegisteredAt { get; set; }
        public string Email { get; set; }
        public TeamDto Team { get; set; }
        public override string ToString()
        {
            return Id + "-" + FirstName + " " + LastName;
        }
    }
}
