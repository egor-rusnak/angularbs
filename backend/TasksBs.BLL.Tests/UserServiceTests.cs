﻿using TasksBs.BLL.Interfaces;
using TasksBs.BLL.Services;
using TasksBs.Common.DTOs;
using TasksBs.TestsCommon;
using Xunit;

namespace TasksBs.BLL.Tests
{
    public class UserServiceTests : DbServicesTest
    {
        private readonly IUserService _userService;

        public UserServiceTests() : base("UserDb")
        {
            _userService = new UserService(_context, _mapper);
        }

        [Fact]
        public async System.Threading.Tasks.Task AddUser_WithRightTeam_UserAdded()
        {
            var team = await _context.AddTeam(); //for no conflicts adding user entity
            var user = DtoCreateTools.CreateUser(team: _mapper.Map<TeamDto>(team));

            var resultUser = await _userService.AddUser(user);

            Assert.NotNull(resultUser);
            Assert.Equal(team.Id, resultUser.Team.Id);
            Assert.Equal(user.FirstName, resultUser.FirstName);
            Assert.Equal(user.LastName, resultUser.LastName);
            Assert.Equal(user.RegisteredAt, resultUser.RegisteredAt);
            Assert.Equal(user.BirthDay, resultUser.BirthDay);
        }
        [Fact]
        public async System.Threading.Tasks.Task AddUser_WithNoExistingTeam_UserAdded()
        {
            var user = DtoCreateTools.CreateUser();

            var resultUser = await _userService.AddUser(user);

            Assert.NotNull(resultUser);
            Assert.Null(resultUser.Team);
        }
    }
}
