﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using TasksBs.BLL.Interfaces;
using TasksBs.BLL.Services;
using TasksBs.TestsCommon;
using Xunit;

namespace TasksBs.BLL.Tests
{
    public class TaskServiceTests : DbServicesTest
    {
        private readonly ITaskService _taskService;
        public TaskServiceTests() : base("TaskDb")
        {
            _taskService = new TaskService(_context, _mapper);
        }

        [Fact]
        public async System.Threading.Tasks.Task SetTaskAsFinished_TaskSettedAsFinished()
        {
            var team = await _context.AddTeam();
            var user = await _context.AddUser(teamId: team.Id);
            var project = await _context.AddProject(user.Id, team.Id);
            var task = await _context.AddTask(user.Id, project.Id);
            await _context.SaveChangesAsync();

            await _taskService.SetTaskAsFinished(task.Id);

            var result = (await _context.Tasks.FirstOrDefaultAsync(t => t.Id == task.Id)).FinishedAt;
            Assert.NotNull(result);
        }

        [Fact]
        public async System.Threading.Tasks.Task SetTaskAsFinished_WrongTaskId_ThrowsArgumentException()
        {
            var team = await _context.AddTeam();
            var user = await _context.AddUser(teamId: team.Id);
            var project = await _context.AddProject(user.Id, team.Id);
            await _context.AddTask(user.Id, project.Id);
            await _context.SaveChangesAsync();

            await Assert.ThrowsAsync<ArgumentException>(async () => await _taskService.SetTaskAsFinished(4505));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUnfinishedTasksByUser_UserWithUnfinishedTasks_ReturnedUnfinishedTasks()
        {
            var team = await _context.AddTeam();
            var user = await _context.AddUser(teamId: team.Id);
            var project = await _context.AddProject(user.Id, team.Id);
            await _context.AddTask(user.Id, project.Id);
            await _context.AddTask(user.Id, project.Id);
            await _context.AddTask(user.Id, project.Id, createdAt: DateTime.Now, finishedAt: DateTime.Now);
            await _context.AddTask(user.Id, project.Id, createdAt: DateTime.Now, finishedAt: DateTime.Now);
            await _context.SaveChangesAsync();

            var result = await _taskService.GetUnfinishedTasksByUser(user.Id);

            Assert.True(result.Count() == 2);
            Assert.All(result, t => Assert.Equal(user.Id, t.Performer.Id));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUnfinishedTasksByUser_NotExistingUser_ThrowsArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await _taskService.GetUnfinishedTasksByUser(99923));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUnfinishedTasksByUser_NoTasksForUser_ReturnedEmptyCollection()
        {
            var team = await _context.AddTeam();
            var user = await _context.AddUser(teamId: team.Id);
            await _context.AddProject(user.Id, team.Id);
            await _context.SaveChangesAsync();

            var result = await _taskService.GetUnfinishedTasksByUser(user.Id);

            Assert.True(result.Count() == 0);
        }

    }
}
