import { UserService } from "./../../services/user.service";
import { Component, OnInit } from "@angular/core";
import { ComponentCanDeactivate } from "src/app/guards/exit-unsaved.guard";
import { User } from "src/app/models/user";
import { ObservableUnsubscribe } from "src/app/shared/observable-unsubscribe";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"]
})
export class UserListComponent extends ObservableUnsubscribe implements OnInit, ComponentCanDeactivate {
  users: User[];
  $loading=true;
  isCreate = false;

  constructor(private userService: UserService) { super(); }

  canDeactivate() {
    if (this.isCreate) {
      return confirm("You are creating user, are you sure, you want to leave? The new user data will be lost.");
    } else {
      return true;
    }
  }

  ngOnInit() {
    this.userService
      .getUsers()
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(users => {
        this.users = users;
        this.$loading=false;
      });
  }

  ngOnDestroy() {
    this.completeSubscription();
  }

  cancelCreate() {
    this.isCreate = false;
  }

  createUser(user: User) {
    this.isCreate = false;
    this.users = this.users.concat(user);
  }
  removeUser(id: number) {
    this.users = this.users.filter(u => u.id !== id);
  }
}
