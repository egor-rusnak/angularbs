import { ExitUnsavedGuard } from "./../guards/exit-unsaved.guard";
import { UserListComponent } from "./user-list/user-list.component";
import { UserComponent } from "./user.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UserDetailsComponent } from "./user-details/user-details.component";


const routes: Routes = [
  {
    path: "", component: UserComponent, children: [
      { path: "", component: UserListComponent, canDeactivate: [ExitUnsavedGuard] },
      { path: ":id", component: UserDetailsComponent, canDeactivate: [ExitUnsavedGuard] }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
