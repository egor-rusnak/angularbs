import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  { path: "projects", loadChildren: () => import("./project/project.module").then(m => m.ProjectModule) },
  { path: "tasks", loadChildren: () => import("./task/task.module").then(m => m.TaskModule) },
  { path: "users", loadChildren: () => import("./user/user.module").then(m => m.UserModule) },
  { path: "teams", loadChildren: () => import("./team/team.module").then(m => m.TeamModule) },
  { path: "**", redirectTo: "projects" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
