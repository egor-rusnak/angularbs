import { Team } from "./team";

export interface User {
  id: number;
  firstName: string;
  lastName: string;
  birthDay: Date;
  registeredAt: Date;
  email: string;
  team: Team;
}

