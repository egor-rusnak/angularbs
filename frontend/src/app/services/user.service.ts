import { User } from "./../models/user";
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { HttpInternalService } from "./http-internal.service";

@Injectable({
  providedIn: "root"
})
export class UserService {
  apiPrefix: string = "/api/users";

  constructor(private httpService: HttpInternalService) { }

  getUsers() {
    return this.httpService
      .getFullRequest<User[]>(this.apiPrefix)
      .pipe(map((response) => response.body));
  }
  getUser(id: number) {
    return this.httpService
      .getFullRequest<User>(this.apiPrefix + "/" + id)
      .pipe(map(resp => resp.body));
  }

  createUser(user: User) {
    return this.httpService.postFullRequest<User>(this.apiPrefix, user);
  }

  updateUser(user: User) {
    return this.httpService.putFullRequest(this.apiPrefix, user);
  }

  deleteUser(id: number) {
    return this.httpService.deleteFullRequest(this.apiPrefix + "/" + id);
  }
}
