import { Team } from "./../models/team";
import { Injectable } from "@angular/core";
import { HttpInternalService } from "./http-internal.service";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class TeamService {
  apiPrefix: string = "/api/teams";

  constructor(private httpService: HttpInternalService) { }

  getTeams() {
    return this.httpService
      .getFullRequest<Team[]>(this.apiPrefix)
      .pipe(map((response) => response.body));
  }
  getTeam(id: number) {
    return this.httpService
      .getFullRequest<Team>(this.apiPrefix + "/" + id)
      .pipe(map(resp => resp.body));
  }

  createTeam(team: Team) {
    return this.httpService.postFullRequest<Team>(this.apiPrefix, team);
  }

  updateTeam(team: Team) {
    return this.httpService.putFullRequest(this.apiPrefix, team);
  }

  deleteTeam(id: number) {
    return this.httpService.deleteFullRequest(this.apiPrefix + "/" + id);
  }
}
