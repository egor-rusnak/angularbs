import { Directive, ElementRef, Input } from "@angular/core";
import { Task } from "../../models/task";

@Directive({
  selector: "[appTask]"
})
export class TaskDirective {
  @Input() task: Task;

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    this.decorate();
  }
  ngOnChanges() {
    this.decorate();
  }

  decorate() {
    if (this.task?.finishedAt)
      this.el.nativeElement.style.backgroundColor = "#BBBBBB";
  }
}
