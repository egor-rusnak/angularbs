import { AppPipesModule } from "./../shared/app-pipes.module";
import { TaskDetailsComponent } from "./task-details/task-details.component";
import { TaskCreateComponent } from "./task-create/task-create.component";
import { TaskDirective } from "./directives/task.directive";
import { TaskItemComponent } from "./task-list/task-item/task-item.component";
import { TaskListComponent } from "./task-list/task-list.component";
import { MaterialModule } from "./../shared/material.module";
import { SharedModule } from "./../shared/shared.module";
import { TaskRoutingModule } from "./task-routing.module";
import { NgModule } from "@angular/core";
import { TaskComponent } from "./task.component";
import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [
    AppPipesModule,
    TaskRoutingModule,
    SharedModule,
    FormsModule,
    MaterialModule,
  ],
  declarations: [TaskComponent, TaskListComponent, TaskItemComponent, TaskDirective, TaskCreateComponent, TaskDetailsComponent]
})
export class TaskModule { }
