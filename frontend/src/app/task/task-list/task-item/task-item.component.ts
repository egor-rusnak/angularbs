import { ObservableUnsubscribe } from "./../../../shared/observable-unsubscribe";
import { TaskService } from "./../../../services/task.service";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Task } from "src/app/models/task";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-task-item",
  templateUrl: "./task-item.component.html",
  styleUrls: ["./task-item.component.scss"]
})
export class TaskItemComponent extends ObservableUnsubscribe implements OnInit {
  @Input("item") task: Task;

  @Output() onRemove: EventEmitter<number> = new EventEmitter<number>();

  constructor(private taskService: TaskService) { super(); }

  ngOnInit() {

  }

  remove() {
    if (confirm("Are you sure to delete task?")) {
      this.taskService.deleteTask(this.task.id)
        .pipe(takeUntil(this.$unsubscribe))
        .subscribe(() => {
          this.onRemove.emit(this.task.id);
        }, error=>alert(error.message));
    }
  }

  finishTask() {
    this.taskService.finishTask(this.task)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(resp =>
        this.taskService.getTask(this.task.id)
          .pipe(takeUntil(this.$unsubscribe))
          .subscribe((t) => this.task = t)
      );
  }


  ngOnDestroy() {
    this.completeSubscription();
  }
}
