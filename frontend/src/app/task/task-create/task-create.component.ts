import { ObservableUnsubscribe } from "./../../shared/observable-unsubscribe";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { takeUntil } from "rxjs/operators";
import { Project } from "src/app/models/project";
import { User } from "src/app/models/user";
import { Task } from "src/app/models/task";
import { UserService } from "src/app/services/user.service";
import { TaskService } from "src/app/services/task.service";

@Component({
  selector: "app-task-create",
  templateUrl: "./task-create.component.html",
  styleUrls: ["./task-create.component.scss"]
})
export class TaskCreateComponent extends ObservableUnsubscribe implements OnInit {
  @Input() project: Project;

  $loading: boolean = true;

  newTask: Task = {} as Task;
  users: User[];
  @Output() created = new EventEmitter<Task>();
  @Output() canceled = new EventEmitter();

  taskForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private taskService: TaskService,
    private userService: UserService
  ) {
    super();
  }


  ngOnInit() {
    this.userService.getUsers()
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe((users) => {
        this.users = users;
        this.$loading = false;
      });
    this.createForm();
  }

  createForm() {
    this.taskForm = this.fb.group({
      name: [""],
      description: [""],
      performer: ["", Validators.required],
      project: [this.project, Validators.required],
      finishedAt: [],
    });
  }

  onSubmit() {
    this.$loading = true;
    this.newTask = this.taskForm.value as Task;
    this.newTask.createdAt = new Date();
    this.taskService
      .createTask(this.newTask)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(
        (p) => {
          this.created.emit(p.body);
          this.$loading = false;
        },
        (er) => {
          console.log(er);
          this.$loading = false;
        }
      );
  }

  ngOnDestroy() {
    this.completeSubscription();
  }

  cancelCreate() {
    this.canceled.emit();
  }

  compareUsers(u1: User, u2: User) {
    return u1.id === u2.id;
  }
}
