import { ComponentCanDeactivate } from "./../../guards/exit-unsaved.guard";
import { ObservableUnsubscribe } from "./../../shared/observable-unsubscribe";
import { ProjectService } from "./../../services/project.service";
import { Component, OnInit } from "@angular/core";
import { Task } from "src/app/models/task";
import { User } from "src/app/models/user";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "src/app/services/user.service";
import { TaskService } from "src/app/services/task.service";
import { takeUntil } from "rxjs/operators";
import { forkJoin, Observable } from "rxjs";
import { Project } from "src/app/models/project";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-task-details",
  templateUrl: "./task-details.component.html",
  styleUrls: ["./task-details.component.scss"]
})
export class TaskDetailsComponent extends ObservableUnsubscribe implements OnInit, ComponentCanDeactivate {

  $loading: boolean = true;

  task: Task;
  performers: User[];
  projects: Project[];

  taskForm: FormGroup;
  isEdit = false;

  constructor(
    private fb: FormBuilder,
    private taskService: TaskService,
    private userService: UserService,
    private projectService: ProjectService,
    private route: ActivatedRoute
  ) {
    super();
  }
  canDeactivate() {
    if (this.isEdit) {
      return confirm("You are editing task, are you sure, you want to leave without saving? The data will be lost.");
    } else {
      return true;
    }
  }


  ngOnInit() {
    const taskId = Number.parseInt(this.route.snapshot.paramMap.get("id"));
    forkJoin([this.userService.getUsers(), this.projectService.getProjects(), this.taskService.getTask(taskId)])
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe((response) => {
        this.performers = response[0];
        this.projects = response[1];
        this.task = response[2];
        this.$loading = false;
      });
  }

  cancelEdit() {
    this.isEdit = false;
  }

  editTask() {
    this.isEdit = true;
    this.createForm();
  }

  createForm() {
    this.taskForm = this.fb.group({
      id: [],
      name: [],
      createdAt: [],
      finishedAt: [],
      project: [Validators.required],
      description: [],
      performer: [Validators.required]
    });
    this.taskForm.setValue(this.task);
  }


  onSubmit() {
    this.$loading = true;
    let task = this.taskForm.value as Task;
    this.taskService
      .updateTask(task)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(
        (t) => {
          this.task = t.body as Task;
          this.$loading = false;
          this.isEdit = false;
        },
        (er) => {
          console.log(er);
          this.$loading = false;
        }
      );
  }

  ngOnDestroy() {
    this.completeSubscription();
  }

  compareUsers(u1: User, u2: User) {
    return u1.id === u2.id;
  }

  compareProjects(p1: Project, p2: Project) {
    return p1.id === p2.id;
  }
}
