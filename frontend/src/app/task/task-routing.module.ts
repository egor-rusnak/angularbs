import { ExitUnsavedGuard } from "./../guards/exit-unsaved.guard";
import { TaskListComponent } from "./task-list/task-list.component";
import { TaskDetailsComponent } from "./task-details/task-details.component";
import { TaskComponent } from "./task.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";


const routes: Routes = [
  {
    path: "", component: TaskComponent, children: [
      { path: "", component: TaskListComponent, canDeactivate: [ExitUnsavedGuard] },
      { path: ":id", component: TaskDetailsComponent, canDeactivate: [ExitUnsavedGuard] }
    ]
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskRoutingModule { }
