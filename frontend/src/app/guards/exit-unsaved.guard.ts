import { CanDeactivate } from "@angular/router";
import { Observable } from "rxjs/internal/Observable";

export interface ComponentCanDeactivate {
  canDeactivate: () => boolean | Observable<boolean>;
}
export class ExitUnsavedGuard implements CanDeactivate<ComponentCanDeactivate> {
  canDeactivate(
    component: ComponentCanDeactivate
  ): Observable<boolean> | boolean {
    return component.canDeactivate ? component.canDeactivate() : true;
  }
}
