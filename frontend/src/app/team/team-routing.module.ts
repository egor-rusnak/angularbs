import { TeamDetailsComponent } from "./team-details/team-details.component";
import { TeamListComponent } from "./team-list/team-list.component";
import { TeamComponent } from "./team.component";
import { ExitUnsavedGuard } from "../guards/exit-unsaved.guard";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";


const routes: Routes = [{
  path: "", component: TeamComponent, children: [
    { path: "", component: TeamListComponent, canDeactivate: [ExitUnsavedGuard] },
    { path: ":id", component: TeamDetailsComponent, canDeactivate: [ExitUnsavedGuard] }
  ]
}]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
