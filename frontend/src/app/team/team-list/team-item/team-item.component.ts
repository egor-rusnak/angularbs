import { TeamService } from "./../../../services/team.service";
import { Team } from "./../../../models/team";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ObservableUnsubscribe } from "src/app/shared/observable-unsubscribe";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-team-item",
  templateUrl: "./team-item.component.html",
  styleUrls: ["./team-item.component.scss"]
})
export class TeamItemComponent extends ObservableUnsubscribe implements OnInit {

  @Input("item") team: Team;

  @Output() onRemove: EventEmitter<number> = new EventEmitter<number>();

  constructor(private teamService: TeamService) { super(); }

  ngOnInit() {

  }

  remove() {
    if (confirm("Are you sure to delete team?")) {
      this.teamService.deleteTeam(this.team.id)
        .pipe(takeUntil(this.$unsubscribe))
        .subscribe(() => {
          this.onRemove.emit(this.team.id);
        }, error => console.log(error.message));
    }
  }

  ngOnDestroy() {
    this.completeSubscription();
  }
}
