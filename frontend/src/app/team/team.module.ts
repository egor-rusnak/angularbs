import { TeamCreateComponent } from "./team-create/team-create.component";
import { TeamDetailsComponent } from "./team-details/team-details.component";
import { TeamItemComponent } from "./team-list/team-item/team-item.component";
import { TeamListComponent } from "./team-list/team-list.component";
import { AppPipesModule } from "./../shared/app-pipes.module";
import { MaterialModule } from "./../shared/material.module";
import { SharedModule } from "./../shared/shared.module";
import { TeamRoutingModule } from "./team-routing.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TeamComponent } from "./team.component";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    AppPipesModule,
    TeamRoutingModule
  ],
  declarations: [TeamComponent, TeamListComponent, TeamItemComponent, TeamDetailsComponent, TeamCreateComponent]
})
export class TeamModule { }
