import { Team } from "./../../models/team";
import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { ObservableUnsubscribe } from "src/app/shared/observable-unsubscribe";
import { TeamService } from "src/app/services/team.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-team-create",
  templateUrl: "./team-create.component.html",
  styleUrls: ["./team-create.component.scss"],
})
export class TeamCreateComponent
  extends ObservableUnsubscribe
  implements OnInit {
  $loading: boolean = true;

  newTeam: Team = {} as Team;

  @Output() created = new EventEmitter<Team>();
  @Output() canceled = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private teamService: TeamService,
  ) {
    super();
  }

  teamForm: FormGroup;

  ngOnInit() {
    this.createForm();
    this.$loading = false;
  }

  createForm() {
    this.teamForm = this.fb.group({
      name: ["", [Validators.required, Validators.minLength(5)]],
    });
  }

  onSubmit() {
    this.$loading = true;
    this.newTeam = this.teamForm.value as Team;
    this.newTeam.createdAt = new Date();
    this.teamService
      .createTeam(this.newTeam)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(
        (p) => {
          this.created.emit(p.body);
          this.$loading = false;
          this.teamForm.reset();
        },
        (er) => {
          console.log(er);
          this.$loading = false;
        }
      );
  }

  ngOnDestroy() {
    this.completeSubscription();
  }

  cancelCreate() {
    this.canceled.emit();
  }
}
