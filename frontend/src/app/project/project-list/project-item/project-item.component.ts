import { ObservableUnsubscribe } from "./../../../shared/observable-unsubscribe";
import { ProjectService } from "./../../../services/project.service";
import { Project } from "./../../../models/project";
import { Component, Input, OnInit, Output } from "@angular/core";
import { EventEmitter } from "@angular/core";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-project-item",
  templateUrl: "./project-item.component.html",
  styleUrls: ["./project-item.component.scss"],
})
export class ProjectItemComponent
  extends ObservableUnsubscribe
  implements OnInit {
  @Input("item") project: Project;

  @Output() onRemove: EventEmitter<number> = new EventEmitter<number>();

  constructor(private projectService: ProjectService) {
    super();
  }

  ngOnInit() { }

  remove() {
    if (confirm("Are you sure to delete project?")) {
      this.projectService
        .deleteProject(this.project.id)
        .pipe(takeUntil(this.$unsubscribe))
        .subscribe(() => {
          this.onRemove.emit(this.project.id);
        }, error=>alert(error.message));
    }
  }

  ngOnDestroy() {
    this.completeSubscription();
  }
}
