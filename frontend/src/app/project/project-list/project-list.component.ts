import { ComponentCanDeactivate } from "./../../guards/exit-unsaved.guard";
import { ObservableUnsubscribe } from "./../../shared/observable-unsubscribe";
import { ProjectService } from "./../../services/project.service";
import { Component, OnInit } from "@angular/core";
import { Project } from "src/app/models/project";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-project-list",
  templateUrl: "./project-list.component.html",
  styleUrls: ["./project-list.component.scss"],
})
export class ProjectListComponent
  extends ObservableUnsubscribe
  implements OnInit, ComponentCanDeactivate {
  isCreate = false;

  $loading = true;
  projects: Project[];

  constructor(private projectService: ProjectService) {
    super();
  }
  canDeactivate() {
    if (this.isCreate) {
      return confirm(
        "You are creating a project, are you shure, you want to leave? The data of new project will be lost."
      );
    } else {
      return true;
    }
  }

  ngOnInit() {
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe((projects) => {
        this.projects = projects;
        this.$loading = false;
      });
  }

  ngOnDestroy() {
    this.completeSubscription();
  }

  onRemoveItem(id: number) {
    this.projects = this.projects.filter((p) => p.id != id);
  }

  onCreated(project: Project) {
    this.projects = this.projects.concat(project);
    this.isCreate = false;
  }
  onCancel() {
    this.isCreate = false;
  }
}
