import { AppPipesModule } from "./../shared/app-pipes.module";
import { ProjectDetailsComponent } from "./project-details/project-details.component";
import { ProjectCreateComponent } from "./project-create/project-create.component";
import { ProjectItemComponent } from "./project-list/project-item/project-item.component";
import { MaterialModule } from "./../shared/material.module";
import { ProjectListComponent } from "./project-list/project-list.component";
import { SharedModule } from "./../shared/shared.module";
import { NgModule } from "@angular/core";
import { ProjectComponent } from "./project.component";
import { ProjectRoutingModule } from "./project-routing.module";

@NgModule({
  imports: [SharedModule, AppPipesModule, ProjectRoutingModule, MaterialModule],
  declarations: [
    ProjectComponent,
    ProjectListComponent,
    ProjectItemComponent,
    ProjectCreateComponent,
    ProjectDetailsComponent
  ],
})
export class ProjectModule { }
