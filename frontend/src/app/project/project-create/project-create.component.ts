import { takeUntil } from "rxjs/operators";
import { ObservableUnsubscribe } from "./../../shared/observable-unsubscribe";
import { User } from "./../../models/user";
import { UserService } from "./../../services/user.service";
import { TeamService } from "./../../services/team.service";
import { ProjectService } from "./../../services/project.service";
import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Project } from "src/app/models/project";
import { Team } from "src/app/models/team";
import { forkJoin } from "rxjs";

@Component({
  selector: "app-project-create",
  templateUrl: "./project-create.component.html",
  styleUrls: ["./project-create.component.scss"],
})
export class ProjectCreateComponent
  extends ObservableUnsubscribe
  implements OnInit {
  $loading: boolean = true;

  newProject: Project = {} as Project;
  teams: Team[];
  users: User[];
  @Output() created = new EventEmitter<Project>();
  @Output() canceled = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private projectService: ProjectService,
    private teamService: TeamService,
    private userService: UserService
  ) {
    super();
  }

  form: FormGroup;

  ngOnInit() {
    forkJoin([this.teamService.getTeams(), this.userService.getUsers()])
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe((r) => {
        this.teams = r[0];
        this.users = r[1];
        this.$loading = false;
      });

    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      name: [""],
      description: [""],
      author: ["", Validators.required],
      team: ["", Validators.required],
      deadline: ["", Validators.required],
    });
  }

  onSubmit() {
    this.$loading = true;
    this.newProject = this.form.value as Project;
    this.newProject.createdAt = new Date();
    this.projectService
      .createProject(this.newProject)
      .pipe(takeUntil(this.$unsubscribe))
      .subscribe(
        (p) => {
          this.created.emit(p.body);
          this.$loading = false;
          this.form.reset();
        },
        (er) => {
          console.log(er);
          this.$loading = false;
        }
      );
  }

  ngOnDestroy() {
    this.completeSubscription();
  }

  cancelCreate() {
    this.canceled.emit();
  }
}
