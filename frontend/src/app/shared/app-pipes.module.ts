import { UserInfoPipe } from "./../pipes/user-info.pipe";
import { UaDatePipe } from "./../pipes/ua-date.pipe";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [UaDatePipe, UserInfoPipe],
  exports: [UaDatePipe, UserInfoPipe]
})
export class AppPipesModule { }
